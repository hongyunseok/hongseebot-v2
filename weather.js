const request = require('request');
const cheerio = require('cheerio');
const urlencode = require('urlencode');
const util = require('util');
// const sharp = require('sharp');
// const fs = require('fs');
// const uuidv1 = require('uuid/v1');

module.exports = (t, callback) => {
    const target = t || "내유동";

    request(util.format("https://search.naver.com/search.naver?query=%s", urlencode(target + " 날씨")), (error, response, body) => {
        if (error) throw error

        let strContents = new Buffer.from(body);

        $ = cheerio.load(strContents.toString('utf-8'));

        const tempS = $('.today_area._mainTabContent').find('.main_info')

        // 오늘 온도
        const temp_result = tempS.find('.info_data').find('.info_temperature').find('.todaytemp').text();
        // 오늘 날씨
        const cast_result = tempS.find('.info_data').find('.info_list').find('.cast_txt').text();
        // 오늘 최저 온도
        const min_temp_result = tempS.find('.info_data').find('.info_list').find('.merge').find('.min').find('.num').text();
        // 오늘 최대 온도  
        const max_temp_result = tempS.find('.info_data').find('.info_list').find('.merge').find('.max').find('.num').text();
        // 미세먼지
        const micro_dust = $('.today_area._mainTabContent').find(".sub_info").find(".detail_box").find(".indicator").find(".num").text();
        // 이미지
        //const img = tempS.find('.ico_state').attr('class').split(' ')[1];

        //console.log(img);

        callback({
            temp: temp_result,
            cast: cast_result,
            min: min_temp_result,
            max: max_temp_result,
            micro: micro_dust,
            //image: getImage(img)
        });

        //https://ssl.pstatic.net/sstatic/keypage/outside/scui/weather_new/img/sp_wt_status_112.png
    });
};

// var getImage = (img) => {
//     var loc;

//     switch (img) {
//         case 'ws1':
//             loc = [0, 0]; // .cs_weather.ico_state.ws1{ background  position: 0 0; } /*맑음(낮)*/

//         case 'ws2':
//             loc = [113, 0]; // .cs_weather.ico_state.ws2{ background  position: 113px 0; } /*맑음(밤)*/

//         case 'ws3':
//             loc = [225, 0]; // .cs_weather.ico_state.ws3{ background  position: 225px 0; } /*구름조금(낮)*/

//         case 'ws4':
//             loc = [337, 0]; // .cs_weather.ico_state.ws4{ background  position: 337px 0; } /*구름조금(밤)*/

//         case 'ws5':
//             loc = [449, 0] // .cs_weather .ico_state.ws5{backgroundposition:449px 0;} /*구름많음(낮)*/

//         case 'ws6':
//             loc = [561, 0]; // .cs_weather.ico_state.ws6{ background  position: 561px 0; } /*구름많음(밤)*/
            
//         case 'ws7':
//             loc = [0, 123]; // .cs_weather.ico_state.ws7{ background  position: 0  123px; } /*흐림*/

//         case 'ws8':
//             loc = [113, 123]; // .cs_weather.ico_state.ws8{ background  position: 113px  123px; } /*약한비*/

//         case 'ws9':
//             loc = [225, 123]; // .cs_weather.ico_state.ws9{ background  position: 225px  123px; } /*비*/

//         case 'ws10':
//             loc = [337, 123]; // .cs_weather.ico_state.ws10{ background  position: 337px  123px; } /*강한비*/

//         case 'ws11':
//             loc = [449, 123]; // .cs_weather.ico_state.ws11{ background  position: 449px  123px; } /*약한눈*/

//         case 'ws12':
//             loc = [561, 123]; // .cs_weather.ico_state.ws12{ background  position: 561px  123px; } /*눈*/

//         case 'ws13':
//             loc = [0, 0]; // .cs_weather.ico_state.ws13{ background  position: 0  235px; } /*강한눈*/

//         case 'ws14':
//             loc = [0, 235]; // .cs_weather.ico_state.ws14{ background  position: 113px  235px; } /*진눈깨비*/

//         case 'ws15':
//             loc = [225, 235]; // .cs_weather.ico_state.ws15{ background  position: 225px  235px; } /*소나기*/

//         case 'ws16':
//             loc = [337, 235]; // .cs_weather.ico_state.ws16{ background  position: 337px  235px; } /*소낙 눈*/

//         case 'ws17':
//             loc = [449, 235]; // .cs_weather.ico_state.ws17{ background  position: 449px  235px; } /*안개*/

//         case 'ws18':
//             loc = [561, 235]; // .cs_weather.ico_state.ws18{ background  position: 561px  235px; } /*번개,뇌우*/

//         case 'ws19':
//             loc = [0, 357]; // .cs_weather.ico_state.ws19{ background  position: 0  357px; } /*우박*/

//         case 'ws20':
//             loc = [113, 357]; // .cs_weather.ico_state.ws20{ background  position: 113px  357px; } /*황사*/

//         case 'ws21':
//             loc = [225, 357]; // .cs_weather.ico_state.ws21{ background  position: 225px  357px; } /*비 또는 눈*/

//         case 'ws22':
//             loc = [337, 357]; // .cs_weather.ico_state.ws22{ background  position: 337px  357px; } /*가끔 비*/

//         case 'ws23':
//             loc = [449, 357]; // .cs_weather.ico_state.ws23{ background  position: 449px  357px; } /*가끔 눈*/

//         case 'ws24':
//             loc = [561, 357]; // .cs_weather.ico_state.ws24{ background  position: 561px  357px; } /*가끔 비 또는 눈*/

//         case 'ws25':
//             loc = [0, 479]; // .cs_weather.ico_state.ws25{ background  position: 0  479px; } /*흐린 후 갬*/

//         case 'ws26':
//             loc = [113, 479]; // .cs_weather.ico_state.ws26{ background  position: 113px  479px; } /*뇌우 후 갬*/

//         case 'ws27':
//             loc = [225, 479]; // .cs_weather.ico_state.ws27{ background  position: 225px  479px; } /*비 후 갬*/

//         case 'ws28':
//             loc = [337, 479]; // .cs_weather.ico_state.ws28{ background  position: 337px  479px; } /*눈 후 갬*/

//         case 'ws29':
//             loc = [449, 479]; // .cs_weather.ico_state.ws29{ background  position: 449px  479px; } /*흐려져 비*/

//         case 'ws30':
//             loc = [561, 479]; // .cs_weather.ico_state.ws30{ background  position: 561px  479px; } /*흐려져 눈*/
//     }

//     var path = './tmp/' + uuidv1() + '.png';
//     fs.createReadStream('weather.png').pipe(sharp().resize(loc[0], loc[1], { width: 112 })).pipe(fs.createWriteStream(path));
//     return path;
// }