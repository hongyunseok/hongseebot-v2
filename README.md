HongSeeBot-v2

# Who is HongSeeBot (홍씨 봇은 누구입니까?)

홍씨봇은 개발자 홍윤석의 홍씨를 따서 홍씨봇이라는 이름을 붙여줬습니다!

# How can I use this?

이 오픈 소스를 활용해서, 즐겁게 텔레그램 봇을 만들어보시길 바랍니다.

# Is this really WTFPL? (이것은 진짜 WTFPL 입니까?)

네, 저희는 라이센스는 개나 줬습니다!

# Libary

cheerio, cjs, delibee, holiday-kr, iconv, isset, moment, moment-timezone, request, strftime, telebot, trim, url, urlencode