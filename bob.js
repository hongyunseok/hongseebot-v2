const request = require("request");
const cheerio = require('cheerio');

const util = require('util');
var Iconv1 = require('iconv').Iconv;

// Time
var strftime = require('strftime');
var year = strftime("%Y");
var month = strftime("%m");
var day = strftime("%d");

module.exports = (msg, callback) => {
    var options = {
        url: util.format("http://gyji.ms.kr/?_page=86&_action=view&_view=view&yy=%s&mm=%s&dd=%s", year, month, day),
        headers: {
          'User-Agent': 'Mozilla/5.0'
        },
        encoding: null
      };

    request(options, (error, response, body) => {
        if(error) throw error
    
        var strContents = new Buffer.from(body);
        iconv = new Iconv1('euc-kr', 'utf-8//translit//ignore');
        strContents = iconv.convert(strContents).toString('utf-8');
        var $ = cheerio.load(strContents);
        
        // console.log(year, month, day);
        
        var today_food = (($('.__LM_APP').find(".day_food").text()).replace(/^\s+|\s+$/g,'')).replace("중식 :", "");
        
        // if (weekDayName == "토요일" || weekDayName == "일요일" || check == true || today_food == undefined)  {
        //     var today_food = "[알림] 오늘은 급식 정보가 없습니다."; 
        // }

        callback({
            today: today_food
        });
    });
};