var isset = require('isset');
const util = require('util');

var delibee = require('delibee')()

//   { EPOST: '우체국택배',
//     CJ: 'CJ대한통운',
//     LOTTE: '롯데택배 (구. 현대택배)',
//     HANJIN: '한진택배',
//     LOGEN: '로젠택배',
//     DREAM: '드림택배 (구. KG로지스)',
//     CVSNET: 'CVSnet 편의점택배',
//     CU: 'CU 편의점택배' } }Pzzzzzzzzzzzzz

module.exports = async (t, callback) => {
    var t = t.split(' ');

    // 변수
    var orcheck = t[1];
    var company = t[2];
    var invoiceNo = t[3];

    // Replace (간단)
    if(company == "CJ대한통운" || company == "대한통운") {
        company = "CJ";
    } else if (company == "롯데택배" || company == "롯데") {
        company = "LOTTE";
    } else if (company == "한진택배" || company == "한진") {
        company = "HANJIN";
    } else if (company == "로젠택배" || company == "로젠") {
        company = "LOGEN";
    } else if (company == "드림택배" || company == "드림" || company == "로지스" || company == "KG로지스") {
        company = "DREAM";
    } else if (company == "CU편의점택배") {
        company = "CU";
    } else if (company == "CVSNET편의점택배") {
        company = "CVSNET"
    }

    if (isset(orcheck) == true && isset(company) == true && isset(invoiceNo) == true) {
        var track_result = await delibee.tracking(company, invoiceNo);

        var track_check = track_result.success; // true or false
        var track_company_name = track_result.invoice.deliveryCompany.name; // CJ대한통운 or 로젠택배 ~~
        var track_status = track_result.invoice.statusText; // "배달중", "배달완료"
        var track_sender_Name = track_result.invoice.senderName;
        // console.log (track_result.invoice.history) // Array
        //console.log (track_result.invoice.history.length) // 1, 2, 3, 4, 5, 6

        var track_history = track_result.invoice.history;
        track_history.forEach(function(location) {
            return ("시간: " + location.dateString + " 위치: " + location.location + " 상태: " + location.remark);
        });

        // console.log (track_history_result);
        

        var track_receiver_Name = track_result.invoice.receiverName;

        if(track_check == false) {
            track_result = "[오류] 정보가 잘못 되었습니다. ex) 택배사, 운송장 번호 체크해주세요."
        } else {
            if (orcheck == "보기") {
                track_result = util.format("택배 회사 명: %s\n보낸 사람: %s | 받는 사람: %s\n배송 상태: %s", track_company_name, track_sender_Name, track_receiver_Name, track_status);
            } else {
                var track_history = track_result.invoice.history;

                track_history.forEach(function(location) {
                    track_result = ("시간: " + location.dateString + " 위치: " + location.location + " 상태: " + location.remark);
                });
                // track_result = util.format("%s", track_history_result);
            }
            
        }

    } else if (isset(orcheck) == true && isset(company) == true && isset(invoiceNo) == false) {
        var track_result = "[오류] 운송장 번호 없음.";
    } else if (isset(orcheck) == true && isset(company) == false && isset(invoiceNo) == false) {
        var track_result = "[알림] 해당 기능을 사용하는 방법은 /택배 보기/기록 <택배 회사> <운송장 번호> 입니다.";
    } else {
        var track_result = "[알림] 해당 기능을 사용하는 방법은 /택배 보기/기록 <택배 회사> <운송장 번호> 입니다.";
    }
    return callback({
        track: track_result,
        company_info: company,
        track_info: invoiceNo,
        track_check: track_check
    });
};
