// Telegram bot
const TeleBot = require('telebot');
const bot = new TeleBot('<Telegram Bot Token>');

// G00d J0b
const util = require('util');

// Includes 
var weather = require('./weather');
var bob = require("./bob.js")
var tracking = require("./tracking.js");

// Check 
var isset = require('isset');
const moment = require('moment');

// 요일을 얻자아ㅏㅏㅏ

// Time
var strftime = require('strftime');
var year = strftime("%Y");
var month = strftime("%m");
var day = strftime("%d");

var weekDayName = moment(year + "-" + month + "-" + day).format('dddd');

if (weekDayName == "Monday") {
    weekDayName = "월요일";
} else if (weekDayName == "Tuesday") {
    weekDayName = "화요일";
} else if (weekDayName == "Wednesday") {
    weekDayName = "수요일";
} else if (weekDayName == "Thursday") {
    weekDayName = "목요일";
} else if (weekDayName == "Friday") {
    weekDayName = "금요일";
} else if (weekDayName == "Saturday") {
    weekDayName = "토요일";
} else if (weekDayName == "Sunday") {
    weekDayName = "일요일";
}

bot.on(['/start', '/hello'], (msg) => msg.reply.text('안녕하세요. 저는 홍씨봇입니다!\n저는 봇을 개발해주신 홍윤석님에게만 도움되는 정보만 제공합니다!\n\n/food or /급식 - 급식을 조회하실 수 있습니다. (고양제일중학교)\n/weather or /날씨 <동네 이름> - 해당 동네의 날씨를 알려드립니다. ex) /날씨 내유동\n/track 보기/기록 <택배 회사> <운송장 번호>'));

bot.on(['/weather', '/날씨'], msg => {
	bot.sendMessage(msg.from.id, '잠시만 기달려주세요!').then((ready) => {
		weather(msg.text, (res) => {
			var message = util.format("오늘의 온도는 %s도 입니다. \n%s, 오늘의 최저 기온은 %s도 이며,\n오늘의 최고 기온은 %s도 입니다.\n%s 정도의 미세먼지가 있을 예정입니다.", res.temp, res.cast, res.min, res.max, res.micro);
			bot.editMessageText({
				chatId: ready.chat.id,
				messageId: ready.message_id
			}, message);
			// bot.sendPhoto(msg.from.id, res.image);
		});
	});
});

bot.on(['/track', '/택배'], msg => {
	bot.sendMessage(msg.from.id, '잠시만 기달려주세요!').then((ready) => {
		tracking(msg.text, (res) => {
      if (res.track_check == true) {
        var message = util.format("요청하신 %s사의 %s 에 대한 정보 입니다. \n\n%s", res.company_info, res.track_info, res.track);
        bot.editMessageText({
          chatId: ready.chat.id,
          messageId: ready.message_id
        }, message);
      } else {
        var message = util.format("[오류] 요청하신 정보가 잘못 되었습니다.");
        bot.editMessageText({
          chatId: ready.chat.id,
          messageId: ready.message_id
        }, message);
      }

		});
	});
});

bot.on(["/food", "/bob", "/급식"], msg => {
  bot.sendMessage(msg.from.id, '잠시만 기달려주세요!').then((ready) => {
    bob(msg.text, (res) => {
      if (isset(res.today) == false) {
        var message = util.format("[알림] 오늘은 급식 정보가 없습니다.");
        bot.editMessageText({
          chatId: ready.chat.id,
          messageId: ready.message_id
      }, message);
      } else {
        if (weekDayName == "목요일") {
          var message = util.format("[알림] 오늘은 특식이닭!\n오늘의 급식은 아래와 같습니다. \n\n%s", res.today);
          bot.editMessageText({
            chatId: ready.chat.id,
            messageId: ready.message_id
        }, message);
        } else {
          var message = util.format("오늘의 급식은 아래와 같습니다. \n\n%s", res.today);
          bot.editMessageText({
            chatId: ready.chat.id,
            messageId: ready.message_id
        }, message);
        }
      }
    });
  });
});

bot.on(['audio', 'sticker'], msg => {
	return bot.sendMessage(msg.from.id, '저희는 음성과 스티커를 지원하지 않습네다.');
});

bot.start();